# What is the Markdown Mediawiki Uploader

Markdown Mediawiki Uploader is a Support Tool for uploading git managed markdown documentation into mediawiki.

# How to use this image

```console
docker run -it --name markdown-mediawiki-test -e WIKI_URL=wiki.example.local -e WIKI_USERNAME=apiuser -e WIKI_PASSWORD='1234' -e WIKI_INDEX_PAGENAME=TestDocuMarkdown markdown-mediawiki-uploader
```

## Environment Variables

When you start the `markdown-mediawiki-uploader` image, you can adjust the configuration by passing one or more environment variables on the `docker run` command line.

### `WIKI_URL`

This variable is mandatory and specifies the Mediawiki url for connection. In the above example, it was set to `wiki.example.local`. Connection is executed via `https`

### `WIKI_USERNAME`

This variable is mandatory and specifies the username that will be set for the Mediawiki api account. In the above example, it was set to `apiuser`.

### `WIKI_PASSWORD`

This variable is mandatory and specifies the password that will be set for the Mediawiki api account. In the above example, it was set to `1234`.

### `WIKI_INDEX_PAGENAME`

This variable is mandatory and specifies the Index Pagename that will be used in Mediawiki. In the above example, it was set to `TestDocuMarkdown`.

### `WIKI_API_PATH`

This variable is optional and allows you to specify the MediaWiki API Path. Default: `/w`

### `WIKI_INDEX_HEADER`

This variable is optional and allows you to specify the Header created in Mediawiki Index Page defined via `WIKI_INDEX_PAGENAME`. 
Default: 
```php
= Content = 
<subpages />
```

### `MARKDOWN_DIRECTORY`

This variable is optional and allows you to specify the Markdown Path. Default: `/opt/markdown`

### `MARKDOWN_IMAGES_DIRECTORY`

This variable is optional and allows you to specify the Markdown Images Path. Default: `/opt/markdown/images`

### `MARKDOWN_INDEX_FILENAME`

This variable is optional and allows you to specify the Markdown Index filename. Default: `index.md`

## Where to Store Data

Important note: There are several ways to store data used by applications that run in Docker containers.

The Docker documentation is a good starting point for understanding the different storage options and variations, and there are multiple blogs and forum postings that discuss and give advice in this area. We will simply show the basic procedure here for the latter option above:

1.	Create a data directory on a suitable volume on your host system, e.g. `/my/own/datadir`.
2.	Start your `markdown-mediawiki-uploader` container like this:

	```console
	$ docker run --name some-markdown-mediawiki-uploader -v /my/own/datadir:/opt/markdown -e WIKI_URL=wiki.example.local -e WIKI_USERNAME=apiuser -e WIKI_PASSWORD='1234' -e WIKI_INDEX_PAGENAME=TestDocuMarkdown markdown-mediawiki-uploader markdown-mediawiki-uploader:tag
	```
