FROM python:slim

RUN apt-get update; \
    apt-get install -y --no-install-recommends pandoc; \
    rm -rf /var/lib/apt/lists/*; \
    pip install mwclient; \
    pip install pypandoc; \
    mkdir /opt/markdown;

VOLUME /opt/markdown

COPY markdown-mediawiki-uploader /usr/local/bin

CMD ["markdown-mediawiki-uploader"]
